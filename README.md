# BennuGD Flatpak

[BennuGD](https://bennugd.org) game engine packaged into a Flatpak. This makes it easy to use BennuGD on any Linux distro.

## Installing

At the moment the only way to use this flatpak is to build it from sources.

## Building from sources

First, install [flatpak-builder](https://docs.flatpak.org/en/latest/flatpak-builder.html). Follow instructions
in the `flatpak-builder` documentation site.

A [Makefile](Makefile) is provided with the following targets:

* `install-requirements`: installs the required flatpak runtime, SDK and toolchain
* `build`: builds the flatpak, but doesn't install it.
* `install`: builds and installs the BennuGD flatpak.
* `uninstall`: uninstall the BennuGD flatpak.
* `clean`: cleans the build directory.

After cloning the repository:

```
make install-requirements
```

Then to build and install the flatpak:

```
make install
```

To run `bgdc` or `bgdi` use:

```
flatpak run org.bennugd.BennuGD bgdc <program.prg>

# Or

flatpak run org.bennugd.BennuGD bgdi <program.dcb>
```

## Building BennuGD with SDL-Compat

Simply set any value to `SDL_COMPAT=1` when invoking make such as:

```
SDL_COMPAT=1 make install
```
