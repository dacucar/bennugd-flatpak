# Bitacora

## Pending

* [ ] Use BennuGD svn as source instead of my own.
* [ ] Pack with `SDL_compat`.
* [ ] Pack `moddesc`.
* [ ] Bootstrap `moddesc`.


## 2022/01/23

I managed to implement a `--sdl-compat` but when I examined the execution of the application
with `LD_DEBUG=libs` I noticed that the libraries in `/lib32/sdl-compat` never get loaded. I believe
flatpak sets `RPATH` when building with autotools. 

## 2022/01/15

I was able to run a simple game with flatpak. The difference against last time is that
I have NVidia drivers installed and that I'm running in an X session.

That could explain why the SDL was unable to initialize the video.

## 2021/10/20

Running `flatpak run /bin/bash --devel`... after some undefined time
the prompt exists, making it difficult to debug why this isn't working.

Changing the target SDK didn't help.

I'm leaning towards some issue with the SDL1.2 and my system, since
I also experienced this on local builds of bennugd.

Will try to use SDL compat.

* [x] Try debugging with `flatpak-builder run <build-folder> <manifest> bash`.
  Same issue.
* [ ] Try running on a x-windows session.

## 2021/10/16

__Segfault__

For some reason, I cannot execute bgdi without getting a seg fault.
Running ourside flatpak, I also had the same issue, but it got
magically fixed, don't know if consequence of an update or what.

__Building in a fresh system__

In order to build 32-bits apps I had to install the following:

```bash
flatpak install org.freedesktop.Sdk
flatpak install org.freedesktop.Sdk.Extension.toolchain-i386
flatpak install org.freedesktop.Sdk.Compat.i386
```

Without it, there would be an error on the autoconf check of
the compiler.

## 2021/08/08

* I had problems with inlined functions in `libdraw.c`.
  Perhaps it has to do with the macro `__GNUC__` not being
  defined or similar.

## 2021/08/04

* I found out autotools auto-generated files were pushed
  to the repo and therefore running default auto-tools would
  not work.
* In order to build properly, I have to pass the --build option,
  otherwise 3rdParty path will not be passed. 
 
  `./configure --build=i686-pc-linux-gnu && make clean && make`

* I was able to run the application using --devel, and I was
  finally able to run the application installing the SDK extensions.
  A question remains: will users need to install this manually?

  
### Files I had to clean up

```
Makefile.in
ar-lib
mdate-sh
py-compile
test-driver
ylwrap
.deps
.dirstamp
autom4te.cache
autoscan.log
autoscan-*.log
aclocal.m4
compile
config.cache
config.guess
config.h.in
config.log
config.status
config.sub
configure
configure.scan
depcomp
install-sh
missing
stamp-h1
ltmain.sh
texinfo.tex
libtool.m4
ltoptions.m4
ltsugar.m4
ltversion.m4
lt~obsolete.m4
Makefile
m4
.deps
```

## 2021/08/02

* The SDL distro in `shared-modules/SDL/` listes include files to be removed
  so it is no wonder they get removed.
* The name of the libraries are different for SDL compat and the one produced
  by the SDL shared module. If I want to replace it, I'll need to subsitute
  it.
* Check the hidden builddir for details about what has been generated and 
  where.

