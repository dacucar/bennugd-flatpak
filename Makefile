SHELL=/bin/sh
FLATPAK_BUILDER_FLAGS=
FLATPAK_REPO=flathub
BUILD_DIR=build
flatpak_build_dir:=$(BUILD_DIR)/flatpak

ifndef SDL_COMPAT
flatpak_id:=org.bennugd.BennuGD
else
flatpak_id:=org.bennugd.BennuGD.SdlCompat
endif

flatpak_builder=flatpak-builder $(flatpak_builder_options) $(flatpak_build_dir) $(flatpak_id).yml
flatpak_builder_options:=--user --force-clean $(FLATPAK_BUILDER_FLAGS)

.PHONY: build
build: 
	$(flatpak_builder) 

.PHONY: install-requirements
arch:=x86_64
runtime:=21.08
install-requirements:
	flatpak install $(FLATPAK_REPO) \
	  org.freedesktop.Platform/$(arch)/$(runtime) \
	  org.freedesktop.Platform.Compat.i386/$(arch)/$(runtime) \
	  org.freedesktop.Sdk/$(arch)/$(runtime) \
	  org.freedesktop.Sdk.Compat.i386/$(arch)/$(runtime) \
	  org.freedesktop.Sdk.Extension.toolchain-i386/$(arch)/$(runtime)

.PHONY: install
flatpak_builder_options+=--install
install: build
	$(flatpak_builder)

.PHONY: enter-flatpak
enter-flatpak:
	flatpak run --command=/bin/bash --devel $(flatpak_id)

.PHONY: uninstall
uninstall: 
	-flatpak remove $(flatpak_id)

.PHONY: clean
clean:
	-rm -r $(BUILD_DIR)
